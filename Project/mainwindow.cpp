#include "mainwindow.h"
#include "ui_mainwindow.h"
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowState(Qt::WindowMaximized);
    QPixmap pix(":/Icon/error.png");
    QIcon icon(pix);
    ui->button4->setIcon(icon);
    ui->button4->setIconSize(pix.size());
    QPixmap pix1(":/Icon/analytics (1).png");
    QIcon icon1(pix1);
    ui->button1->setIcon(icon1);
    ui->button1->setIconSize(pix1.size());
}
void MainWindow::graphic()
{
    Graphic *window=new Graphic();
    window->show();
}
void MainWindow::simulation()
{
    Simulation *window=new Simulation();
    window->show();
}
MainWindow::~MainWindow()
{
    delete ui;
}
