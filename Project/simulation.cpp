#include "simulation.h"
#include<iostream>
using namespace std;
Simulation::Simulation()
{
 setWindowState(Qt::WindowMaximized);
 native = new Widget(this);
 timer = new QTimer(this);
 QGridLayout *layout = new QGridLayout;
 QGridLayout*layout1=new QGridLayout;
 button=new QPushButton("Start");
 QPushButton *button1=new QPushButton("exit");
 button2=new QPushButton("Stop");
 QLineEdit *entry=new QLineEdit;
 QHBoxLayout*hbox=new QHBoxLayout;
 layout1->addWidget(entry);
 layout1->addWidget(button);
 layout1->addWidget(button2);
 layout1->addWidget(button1);
 layout->addWidget(native);
 hbox->addLayout(layout1);
 hbox->addLayout(layout);
 setLayout(hbox);
 QString text=entry->text();
 native->animate();
 connect(button,&QPushButton::clicked,this,&Simulation::startAnimation);
 connect(button1,&QPushButton::clicked,this,&Simulation::close);
 connect(button2,&QPushButton::clicked,this,&Simulation::stopAnimation);
 connect(timer, &QTimer::timeout, native, &Widget::animate);
 button2->close();

}
void Simulation::startAnimation()
{
    timer->start(1);
    button->close();
    button2->show();
}
void Simulation::stopAnimation()
{
    timer->stop();
    button->show();
    button2->close();
}
Simulation::~Simulation()
{
}

