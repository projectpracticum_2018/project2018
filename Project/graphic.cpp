#include "graphic.h"
#include "ui_graphic.h"

Graphic::Graphic(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Graphic)
{
    ui->setupUi(this);
    setWindowState(Qt::WindowMaximized);
    setWindowTitle("Ploting");

}
Graphic::~Graphic()
{
    delete ui;
}

void Graphic::on_pushButton_clicked()
{
    QString str=ui->lineEdit->text();
    ui->verticalWidget->getValue(str);
    ui->verticalWidget->update();
}
//double static i=0.0;
//double static j=0.0;
//painter.drawEllipse(QPointF(-200+i,-j),10,10);
//i=i+0.01;
//if(i>400)
//    i=0.0;
//j=j+0.01;
//if(j>200)
//j=0.0;
//painter.drawLine(QPointF(-300,0),QPointF(200,0));
