#include "widget.h"
#include<cmath>
Widget::Widget(QWidget*parent):QWidget(parent){
  setFixedSize(1200, 768);
  background=QBrush(QColor("#696969"));
  color1=QBrush(QColor("#FF1493"));
  color2=QBrush(QColor("#0000FF"));
  color3=QBrush(QColor("#7CFC00"));
  color4=QBrush(QColor("#2F4F4F"));
  car=new Car(this);
}
void Widget::paintEvent(QPaintEvent *event){
    QPainter painter;
    painter.begin(this);
    painter.fillRect(event->rect(),background);
    painter.translate(1366/2,768/2);
    //painter.scale(3,3);
    painter.setBrush(color2);
    painter.setBrush(color4);
    painter.drawRect(-800,-5,10000,100);
    painter.setRenderHint(QPainter::Antialiasing);
//       static int i=0;
//       QRect b(++i-0.3,10,40,30);
//        if(i>800)
//               i=-800;
//        static  int j=0;
//        j=j+2;
//       QRect a(800-j,50,100,45);
//         if(j>1600)
//               j=0;
       painter.setBrush(color2);
//       painter.drawRect(a);
       painter.setBrush(color1);
//       painter.drawRect(b);
       QPoint st(-1000,-10);
       QPoint end(1000,-10);

       st.setY(45);
       end.setY(45);
       QPen pen1(Qt::yellow);
       pen1.setWidth(2);
       pen1.setCapStyle(Qt::RoundCap);
       QVector<qreal> dashes;
       qreal space = 4;
       dashes << 1 << space<<10<<space;
       pen1.setDashPattern(dashes);
       painter.setPen(pen1);
       painter.drawLine(st, end);
       painter.scale(4,4);
       static double t=0;
       double Vx,Vy;
       double Vox=50,Voy=50;
       Vx=Vox*0.86;
       Vy=Voy*0.5;
       double xo=-100,yo=0.0;
       double x,y;
       x=xo+Vx*t;
       y=yo+Vy*t+(1/2*(-32.2)*pow(t,2));
        if(t>10)
           t=0;
       painter.drawEllipse(QPointF(x,-y),0.1,0.1);
       t=t+0.01;
       car->paintEvent(&painter,event);
       painter.end();
}
void Widget::animate(){
    update();
}
void Widget::setValue(){}
